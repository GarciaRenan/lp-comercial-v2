import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

const API_UPLOAD = environment.API_ENV2;

@Injectable({
  providedIn: 'root'
})
export class UploadImgService {

  constructor(private _http: HttpClient) { }

  setUpload(arq: FormData): Observable<any> {
    return this._http.post<any>(API_UPLOAD + '/api/setimg', arq);
  }
}

import { Component, Input } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';

import { AlertComponent } from './dialog/alert/alert.component';
import { DialogComponent } from './dialog/confirm/dialog.component';
import { Cliente } from './models/cliente';
import { ClienteService } from './services/cliente.service';
import { UploadImgService } from './services/upload-img.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  real = Intl.NumberFormat('pt-BR');
  step = 0;

  setStep(index: number) {
    this.step = index;
  }

  nextStep() {
    this.step++;
  }

  prevStep() {
    this.step--;
  }

  @Input() cod_cliente!: string;
  @Input() nome_loja!: string;
  @Input() cnpj!: string;
  @Input() ano_ini!: string;
  @Input() solicita_motivo_limite: boolean = false;
  @Input() show_alert: boolean = false;
  @Input() maisDeUmaLoja: string = 'não';
  @Input() maisDeUmCanal: string = 'sim';
  @Input() gost_aumento: string = 'não';
  @Input() teste_limite_maior: boolean = true;

  @Input() faturamento: string = '0';
  @Input() faturamento_int: number = 0;
  @Input() futura: string = '0';
  @Input() futura_int: number = 0;
  @Input() limite_cred: number = 0;
  @Input() limite_str: string = '';
  @Input() potencial: number = 0;
  @Input() rep: string = 'Representante';
  @Input() share_verificado: number = 0;
  @Input() limite_mercado: number = 0;
  @Input() limite_verificado: number = 0;

  selectedValue1: number = 0;
  selectedValue2: number = 0;
  numbers1: number[] = [0];
  numbers2: number[] = [0];

  constructor(
    public dialog: MatDialog,
    private _cliente: ClienteService,
    private _upload: UploadImgService
  ) {}

  clientes = new FormGroup({
    cod_cli: new FormControl('', Validators.required),
    cod_grupo: new FormControl(),
    nome_loja: new FormControl(
      { value: '', disabled: true }
    ),
    cnpj: new FormControl(),
    ano_inauguracao: new FormControl('', Validators.required),
    email: new FormControl(
      ' ',
      Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$')
    ),
    instagram_pessoal: new FormControl(),
    instagram_vendedores: new FormControl(),

    possui_canal_de_venda: new FormControl('sim', Validators.required),
    canais_digitais: new FormControl(),
    endereco_canal_ecommerce: new FormControl(),
    endereco_canal_instagram: new FormControl(),
    endereco_canal_facebook: new FormControl(),
    endereco_canal_whatsapp: new FormControl(),
    canal_venda_interessados: new FormControl(),
    canal_venda_mais_vendas: new FormControl(),
    canal_vendas_dificuldade: new FormControl(),

    essencial_marca_ativa: new FormControl('', Validators.required),
    momento_maior_vendas: new FormControl('', Validators.required),
    essencial_compra_colecao: new FormControl('', Validators.required),
    razoes_vender_muito: new FormControl('', Validators.required),
    marketing_sente_falta: new FormControl('', Validators.required),
    marketing_ajuda_muito: new FormControl('', Validators.required),

    possui_mais_de_uma_loja: new FormControl('', Validators.required),
    qnt_lojas: new FormControl(),
    nomes_lojas1: new FormControl('', Validators.required),
    local1: new FormControl('', Validators.required),
    vitrine_expositor1: new FormControl('', Validators.required),
    tamanho_loja1: new FormControl('', Validators.required),
    qnt_vendedores1: new FormControl('', Validators.required),
    local2: new FormControl(),
    nomes_lojas2: new FormControl(),
    vitrine_expositor2: new FormControl(),
    tamanho_loja2: new FormControl(),
    qnt_vendedores2: new FormControl(),
    local3: new FormControl(),
    nomes_lojas3: new FormControl(),
    vitrine_expositor3: new FormControl(),
    tamanho_loja3: new FormControl(),
    qnt_vendedores3: new FormControl(),
    local4: new FormControl(),
    nomes_lojas4: new FormControl(),
    vitrine_expositor4: new FormControl(),
    tamanho_loja4: new FormControl(),
    qnt_vendedores4: new FormControl(),
    local5: new FormControl(),
    nomes_lojas5: new FormControl(),
    vitrine_expositor5: new FormControl(),
    tamanho_loja5: new FormControl(),
    qnt_vendedores5: new FormControl(),
    local6: new FormControl(),
    nomes_lojas6: new FormControl(),
    vitrine_expositor6: new FormControl(),
    tamanho_loja6: new FormControl(),
    qnt_vendedores6: new FormControl(),
    qual_publico: new FormControl('', Validators.required),
    mix_ofertado: new FormControl(),
    percentual_fashion: new FormControl(0, Validators.required),
    percentual_esporte: new FormControl(0, Validators.required),
    percentual_acessorios: new FormControl(0, Validators.required),
    percentual_calcados: new FormControl(0, Validators.required),
    percentual_praia: new FormControl(0, Validators.required),
    percentual_noite: new FormControl(0, Validators.required),

    faturamento_mes: new FormControl('', Validators.required),
    limite: new FormControl(),
    qnt_marcas: new FormControl(1, Validators.required),
    nome_marcas1: new FormControl(
      { value: 'Labellamafia', disabled: true },
      Validators.required
    ),
    tempo_com_marca1: new FormControl(),
    qnt_percentual1: new FormControl(100, Validators.required),
    nome_marcas2: new FormControl(),
    tempo_com_marca2: new FormControl(),
    qnt_percentual2: new FormControl(),
    nome_marcas3: new FormControl(),
    tempo_com_marca3: new FormControl(),
    qnt_percentual3: new FormControl(),
    nome_marcas4: new FormControl(),
    tempo_com_marca4: new FormControl(),
    qnt_percentual4: new FormControl(),
    nome_marcas5: new FormControl(),
    tempo_com_marca5: new FormControl(),
    qnt_percentual5: new FormControl(),
    nome_marcas6: new FormControl(),
    tempo_com_marca6: new FormControl(),
    qnt_percentual6: new FormControl(),
    nome_marcas7: new FormControl(),
    tempo_com_marca7: new FormControl(),
    qnt_percentual7: new FormControl(),
    nome_marcas8: new FormControl(),
    tempo_com_marca8: new FormControl(),
    qnt_percentual8: new FormControl(),
    nome_marcas9: new FormControl(),
    tempo_com_marca9: new FormControl(),
    qnt_percentual9: new FormControl(),
    nome_marcas10: new FormControl(),
    tempo_com_marca10: new FormControl(),
    qnt_percentual10: new FormControl(),
    nome_marcas11: new FormControl(),
    tempo_com_marca11: new FormControl(),
    qnt_percentual11: new FormControl(),
    nome_marcas12: new FormControl(),
    tempo_com_marca12: new FormControl(),
    qnt_percentual12: new FormControl(),
    nome_marcas13: new FormControl(),
    tempo_com_marca13: new FormControl(),
    qnt_percentual13: new FormControl(),
    nome_marcas14: new FormControl(),
    tempo_com_marca14: new FormControl(),
    qnt_percentual14: new FormControl(),
    nome_marcas15: new FormControl(),
    tempo_com_marca15: new FormControl(),
    qnt_percentual15: new FormControl(),
    share: new FormControl({ value: 0, disabled: true }, Validators.required),

    pedido_futura: new FormControl(''),
    limite_exedido: new FormControl(''),
    solicita_aumento: new FormControl('', Validators.required),
    motivo_pedido_menor: new FormControl(''),
    motivo_pedido_maior: new FormControl(''),
    motivo: new FormControl(''),
    aceita_avaliar_pedido: new FormControl('sim', Validators.required),
  });

  getClientes() {
    this._cliente.getCliente(this.cod_cliente).subscribe({
      next: (res: Cliente) => {
        this.nome_loja = res.nome_fantasia_f;
        this.cnpj = res.cnpj_f;
        this.limite_str = res.Limite;
        this.potencial = res.Potencial;

        this.limite_cred = parseFloat(this.limite_str);
        this.rep = res.Representante;

        this.setNome(this.nome_loja, this.cnpj, this.limite_cred);

        this.show_alert = false;
      },
      error: (err) => {
        this.rep = 'Representante';
        this.limite_cred = 0;

        this.setNome('Recém cadastrado', '0', 0);

        // console.log('cod não encontrado', err);
        this.show_alert = true;
      },
    });
    console.log(this.clientes.get('nome_loja')!.value);
  }

  setNome(nome_fantasia_f: string, cnpj_f: string, limite: number) {
    this.clientes.patchValue({
      nome_loja: nome_fantasia_f,
      cnpj: cnpj_f,
      limite: limite,
    });
  }

  get testEmail() {
    return this.clientes.get('email');
  }

  replaceAll(str: string, de: string, para: string) {
    const splitado = str.split(de);
    return splitado.join(para);
  }

  verificaFaturamento() {

    this.faturamento_int = parseInt(this.replaceAll(this.faturamento, ".", ""));
    this.futura_int = parseInt(this.replaceAll(this.futura, ".", ""));
    console.log(this.limite_cred);
    console.log(this.futura_int);

    this.limite_mercado = this.faturamento_int / 2.2;

    // share
    this.share_verificado =
      this.limite_mercado * (this.clientes.get('qnt_percentual1')!.value / 100);
    this.share_verificado = parseFloat(this.share_verificado.toFixed(2));

    this.limite_verificado = this.share_verificado * 3;

    this.clientes.patchValue({ share: this.share_verificado.toFixed(2) });
    this.solicita_motivo_limite = false;

    if (this.limite_cred > this.futura_int) {
      this.teste_limite_maior = true;
    }else {
      this.teste_limite_maior = false;
    }
  }

  getQntLojas() {
    if (this.selectedValue2 > 6) {
      this.selectedValue2 = 6;
    }
    for (var i = 0; i < this.selectedValue2; i++) {
      this.numbers2[i] = i;
    }
  }
  getQntMarcas() {
    if (this.selectedValue1 > 15) {
      this.selectedValue1 = 15;
    }
    for (var i = 0; i < this.selectedValue1; i++) {
      this.numbers1[i] = i;
    }
  }

  formatLabel(value: number) {
    if (value >= 0) {
      return value + '%';
    }

    return value;
  }

  uploadImg(event: any) {
    if (event.target.files && event.target.files[0]) {
      const filename = event.target.files[0];
      const ext = filename.name.split('.')[1];

      const ALLOWED_EXTENSIONS = ['pdf', 'png', 'jpg', 'jpeg'];

      if (ALLOWED_EXTENSIONS.includes(ext.toLowerCase())) {
        const formData = new FormData();
        formData.append('arq', filename);

        this._upload.setUpload(formData).subscribe({
          next: (n) => {
            console.log('Arquivo enviado!');
          },
          error: (e) => {
            console.error(e);
          },
        });
      } else {
        this.dialog.open(AlertComponent);
      }
    }
  }

  // addRowMarcas(item: any) {
  //   const elem = document.querySelector('marcas-wrapper');

  //   // elem?.remove();
  // }
  // removeRowsMarcas() {
  //   const elem = document.getElementById('row'+item);
  //   elem?.remove();
  // }
  // deleteRowLoja() {
  //   const elem = document.getElementById('row'+item);
  //   elem?.remove();
  // }

  salvar() {
    const dialogRef = this.dialog.open(DialogComponent, {
      data: {
        message: 'Salvar formulário?',
        buttonText: {
          ok: 'Sim',
          cancel: 'Cancelar',
        },
      },
    });
    dialogRef.afterClosed().subscribe((confirmed: boolean) => {
      if (confirmed) {
        console.log('1', this.clientes.value);
        this.clientes.setControl('nome_loja', new FormControl());
        this.clientes.setControl('nome_marcas1', new FormControl());
        this.clientes.setControl('share', new FormControl());
        this.clientes.patchValue({
          nome_loja: this.nome_loja,
          nome_marcas1: 'Labellamafia',
          share: this.share_verificado.toFixed(2),
        });
        console.log('2', this.clientes.value);
        this._cliente.setCliente(this.clientes.value).subscribe({
          next: (v) => {
            this._cliente.message('Formulário salvo com sucesso!');
            window.location.reload();
          },
          error: (e) => {
            console.error(e);
            this._cliente.message('Erro ao enviar o formulário!');
          },
        });
      }
    });
  }

  cancelar() {
    this.clientes.reset();
    this.clientes.patchValue({
      nome_marcas1: 'Labellamafia',
      qnt_percentual1: 100,
    });
  }
}
